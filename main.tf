provider "aws" {
  version = "> 2.30.0"
  region  = var.region
}

resource aws_vpc "catweb" {
  cidr_block           = var.cidr_block
  enable_dns_hostnames = true

  tags = {
    name = "${var.prefix}-vpc"
  }
}

resource aws_subnet "catweb" {
  vpc_id     = aws_vpc.catweb.id
  cidr_block = var.subnet_prefix

  tags = {
    name = "${var.prefix}-subnet"
  }
}

resource aws_security_group "catweb" {
  name = "${var.prefix}-security-group"

  vpc_id = aws_vpc.catweb.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name = "${var.prefix}-security-group"
  }
}

resource random_id "app-server-id" {
  prefix      = "${var.prefix}-catweb"
  byte_length = 8
}

resource aws_internet_gateway "catweb" {
  vpc_id = aws_vpc.catweb.id

  tags = {
    Name = "${var.prefix}-internet-gateway"
  }
}

resource aws_route_table "catweb" {
  vpc_id = aws_vpc.catweb.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.catweb.id
  }
}

resource aws_route_table_association "catweb" {
  subnet_id      = aws_subnet.catweb.id
  route_table_id = aws_route_table.catweb.id
}

data aws_ami "ubuntu" {
  most_recent = true

  filter {
    name = "name"
    #values = ["ubuntu/images/hvm-ssd/ubuntu-disco-19.04-amd64-server-*"]
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_eip" "catweb" {
  instance = aws_instance.catweb.id
  vpc      = true
}

resource "aws_eip_association" "catweb" {
  instance_id   = aws_instance.catweb.id
  allocation_id = aws_eip.catweb.id
}

resource aws_instance "catweb" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.catweb.key_name
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.catweb.id
  vpc_security_group_ids      = [aws_security_group.catweb.id]

  tags = {
    Name = "${var.prefix}-catweb-instance"
    Department = "devops team"
    Billable = "true"
  }
}

# We're using a little trick here so we can run the provisioner without
# destroying the VM. Do not do this in production.

# If you need ongoing management (Day N) of your virtual machines a tool such
# as Chef or Puppet is a better choice. These tools track the state of
# individual files and can keep them in the correct configuration.

# Here we do the following steps:
# Sync everything in files/ to the remote VM.
# Set up some environment variables for our script.
# Add execute permissions to our scripts.
# Run the deploy_app.sh script.

resource "null_resource" "configure-cat-app" {
  depends_on = [aws_eip_association.catweb]

  triggers = {
    build_number = timestamp()
  }

  provisioner "file" {
    source      = "files/"
    destination = "/home/ubuntu/"

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = tls_private_key.catweb.private_key_pem
      host        = aws_eip.catweb.public_ip
    }
  }

  provisioner "remote-exec" {
    inline = [
      "sudo add-apt-repository \"deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) main universe restricted multiverse\"",
      "sudo apt -y update",
      "sudo apt -y install apache2 apache2-bin apache2-utils",
      "sudo systemctl start apache2",
      "sudo chown -R ubuntu:ubuntu /var/www/html",
      "chmod +x *.sh",
      "PLACEHOLDER=${var.placeholder} WIDTH=${var.width} HEIGHT=${var.height} PREFIX=${var.prefix} ./deploy_app.sh",
    ]

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = tls_private_key.catweb.private_key_pem
      host        = aws_eip.catweb.public_ip
    }
  }
}

resource tls_private_key "catweb" {
  algorithm = "RSA"
}

locals {
  private_key_filename = "${var.prefix}-ssh-key.pem"
}

resource aws_key_pair "catweb" {
  key_name   = local.private_key_filename
  public_key = tls_private_key.catweb.public_key_openssh
}
