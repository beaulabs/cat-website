#!/bin/bash
# Script to deploy a very simple web application.
# The web app has a customizable image and some text.

cat << EOM > /var/www/html/index.html
<html>
  <head><title>Meow!</title></head>
  <body>
  <div style="width:800px;margin: 0 auto">
  <!-- BEGIN -->
  <center><img src="http://${PLACEHOLDER}/${WIDTH}/${HEIGHT}"></img></center>
  <center><h2>Meow World!</h2></center>
  <center><h2>I'm a cat! If I could text you back...I wouldn't!"</h2></center>
  <h2></h2>
  <center><h2>And your code...How did this even compile?</center></h2>
  <center><img src="https://media.giphy.com/media/3oKIPnAiaMCws8nOsE/giphy.gif"></img></center>
  <!-- END -->
  </div>
  </body>
</html>
EOM

echo "Script complete."